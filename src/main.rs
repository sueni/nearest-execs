use std::fs;
use std::io::{self, Write};
use std::path::{Path, PathBuf};

struct Slicer {
    execs: Vec<PathBuf>,
    dirs: Vec<PathBuf>,
}

impl Slicer {
    pub fn new() -> Self {
        Self {
            execs: Vec::new(),
            dirs: Vec::new(),
        }
    }

    pub fn scan_dir(&mut self, path: &Path) -> io::Result<()> {
        for entry in fs::read_dir(path)? {
            let entry = entry?;
            let path = entry.path();

            if is_executable_file(&path)? {
                self.execs.push(path);
            } else if is_relevant_dir(&path)? {
                self.dirs.push(path);
            }
        }
        Ok(())
    }

    fn slice(&mut self) -> io::Result<()> {
        if !self.execs.is_empty() {
            return Ok(());
        }

        let dirs: Vec<_> = self.dirs.drain(..).collect();

        for dir in dirs.into_iter() {
            self.scan_dir(&dir)?;
        }

        if self.dirs.is_empty() {
            return Ok(());
        }

        self.slice()?;
        Ok(())
    }

    fn display_execs(&self) {
        let stdout = io::stdout();
        let lock = stdout.lock();
        let mut handle = io::BufWriter::new(lock);

        for exec in &self.execs {
            writeln!(handle, "{}", exec.display()).unwrap();
        }
    }
}

fn is_executable_file(path: &Path) -> io::Result<bool> {
    use std::os::unix::fs::PermissionsExt;
    Ok(path.is_file() && path.metadata()?.permissions().mode() & 0o111 != 0)
}

fn is_relevant_dir(path: &Path) -> io::Result<bool> {
    Ok(path.is_dir() && !path.ends_with(".git"))
}

fn main() -> io::Result<()> {
    let start_dir = Path::new(".");
    let mut slicer = Slicer::new();
    slicer.scan_dir(start_dir)?;
    slicer.slice()?;
    slicer.display_execs();
    Ok(())
}
